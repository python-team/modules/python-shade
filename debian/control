Source: python-shade
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Clint Byrum <spamaps@debian.org>
Build-Depends: debhelper-compat (= 13),
    dh-python,
    pybuild-plugin-pyproject,
    python3-all,
    python3-setuptools,
    python3-pbr,
    python3-sphinx,
    python3-testtools,
    python3-stestr (>= 2.3.1-1~),
    python3-dogpile.cache,
    python3-os-client-config (>=1.28.0),
    python3-mock,
    python3-testscenarios,
    python3-openstacksdk (>=0.15.0),
    python3-munch,
    python3-keystoneauth1,
    python3-jsonpatch,
    python3-six,
    python3-munch,
    python3-requestsexceptions (>= 1.1.1),
    python3-decorator,
    python3-betamax,
    python3-requests-mock,
    python3-argparse-manpage,
Standards-Version: 3.9.8
Homepage: https://pypi.python.org/pypi/shade
Vcs-Git: https://salsa.debian.org/python-team/packages/python-shade.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-shade

Package: python3-shade
Architecture: all
Depends: ${misc:Depends},${python3:Depends}
Description: Client library for operating OpenStack clouds
 Shade is a simple Python client library for operating OpenStack
 clouds. The key word here is simple. Clouds can do many things - but
 there are probably only about 10 of them that most people care about
 with any regularity. If you want to do complicated things, you should
 probably use the lower level client libraries - or even the REST API
 directly. However, if what you want is to be able to write an application
 that talks to clouds no matter what choices the deployer has made then
 shade is for you.
 .
 This package installs the library for Python 3.

Package: shade-inventory
Architecture: all
Depends: ${misc:Depends},${python3:Depends},python3-shade (= ${source:Version})
Suggests: ansible
Description: Ansible inventory script for OpenStack clouds
 Shade is a simple Python client library for operating OpenStack
 clouds. The key word here is simple. Clouds can do many things - but
 there are probably only about 10 of them that most people care about
 with any regularity. If you want to do complicated things, you should
 probably use the lower level client libraries - or even the REST API
 directly. However, if what you want is to be able to write an application
 that talks to clouds no matter what choices the deployer has made then
 shade is for you.
 .
 This package installs a script that uses shade to query OpenStack
 clouds and produce a JSON inventory that is suitable for use as an
 Ansible inventory plugin.
