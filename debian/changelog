python-shade (1.33.0-1) UNRELEASED; urgency=medium

  * Team upload.
  * New upstream version 1.33.0
  * remove patch applied upstream

  [ Clint Byrum ]
  * New upstream version 1.30.0

 -- Alexandre Detiste <tchet@debian.org>  Mon, 26 Feb 2024 20:56:22 +0100

python-shade (1.30.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:59:49 -0400

python-shade (1.30.0-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support (Closes: #934336).
  * Fix stestr CLI name.

  [ Andrey Rahmatullin ]
  * Fix building with current dogpile.

 -- Andrey Rahmatullin <wrar@debian.org>  Thu, 22 Aug 2019 00:18:32 +0500

python-shade (1.30.0-2) unstable; urgency=medium

  * Team upload.
  * Completing d/copyright (Closes: #918671)

 -- Stewart Ferguson <stew@ferg.aero>  Sat, 12 Jan 2019 16:01:01 +0100

python-shade (1.30.0-1) unstable; urgency=medium

  [ Clint Byrum ]
  * New upstream release. (Closes: #882551)
  * add new build deps for upstream release

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Clint Byrum ]
  * change from testrepository to stestr per upstream change
  * split shade-inventory into new package and add man page
    (LP: #1770300) (Closes: #843353)
  * Updating Build-Depends based on upstream changes.

 -- Clint Byrum <spamaps@debian.org>  Mon, 07 Jan 2019 04:38:55 -0800

python-shade (1.7.0-2) unstable; urgency=medium

  * Updated standards to v3.9.8, no changes required.
  * Add reference to /usr/share/common-licenses to address copyright-
    should-refer-to-common-license-file-for-apache-2 lintian error.
  * Adding python3 support.
    - Removing erroneous os-cloud-config dependency (shade doesn't
      relate to TripleO)
    - Replaced bunch with munch in build depends per upstream changes
  * Set DPMT as maintainer, demote me to Uploaders.

 -- Clint Byrum <spamaps@debian.org>  Wed, 02 Nov 2016 09:26:17 -0700

python-shade (1.7.0-1) unstable; urgency=medium

  [ Clint Byrum ]
  * New Upstream release. (Closes: #821197)
  * Add missing short license name to debian/copyright

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Clint Byrum ]
  * Newer version of os-client-config is required

 -- Clint Byrum <spamaps@debian.org>  Thu, 19 May 2016 06:29:37 -0700

python-shade (1.3.0-1) unstable; urgency=medium

  * New Upstream release.
  * Fixed SCM to point at git (Closes: #804548)
  * Respect DEB_BUILD_OPTOINS (Closes: #804550)
  * Add new build deps to pass test suite in new release.

 -- Clint Byrum <spamaps@debian.org>  Sat, 26 Dec 2015 07:48:24 -0800

python-shade (0.6.1-1) unstable; urgency=low

  * Initial release. Closes: #784710

 -- Clint Byrum <spamaps@debian.org>  Thu, 14 May 2015 15:22:27 -0700
